<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2; // or kartik\widgets\Select2
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Question */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Ask');
?>
<br>
<div class="col-md-9 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-body"><br>
            <div class="question-form">
                <?php $form = ActiveForm::begin(['action' => 'create', 'method' => 'post']); ?>

                <?= $form->field($model, 'text', ['template' => '<div class="form-group">{label}{input}{error}</div>', 'options' => ['class' => 'col-md-12'], 'labelOptions' => ['class' => 'control-label']])->textInput(['autocomplete' => 'off'])->label(Yii::t('app', 'Write your question').':') ?>

                <?= $form->field($model, 'information', ['template' => '<div class="form-group">{label}{input}{error}</div>', 'options' => ['class' => 'col-md-12'], 'labelOptions' => ['class' => 'control-label']])->textInput(['autocomplete' => 'off'])->label(Yii::t('app', 'Additional information').':') ?>

                <p class="text-center text-info"><?= Yii::t('app', 'Do you know the category of your question?')?></p>

                <?= $form->field($model, 'id_category', ['template' => '<div class="form-group">{label}{input}{error}</div>', 'options' => ['class' => 'col-md-12'], 'labelOptions' => ['class' => 'control-label']])->dropDownList($categories, ['prompt' => Yii::t('app', 'Select a category')])->label(Yii::t('app', 'Select a category').':') ?>

                <p class="text-center text-info"><?= Yii::t('app', 'Which email do we respond to?')?></p>

                <?= $form->field($model, 'email_answer', ['template' => '<div class="form-group">{label}{input}{error}</div>', 'options' => ['class' => 'col-md-12'], 'labelOptions' => ['class' => 'control-label']])->textInput(['type' => 'email'])->label(Yii::t('app', 'Email for reply').':') ?>

                <div class= "form-group col-md-12 text-center">
                    <?= Html::submitButton(Yii::t('app', 'Ask'), ['class' => 'btn btn-success']) ?>
                    <a href="<?= Yii::$app->homeUrl ?>" class="btn btn-default"><?= Yii::t('app', 'Return') ?></a>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>