<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
  // echo var_dump($question->idProfessional->shortFullName);die();
  $text = $question->text;
  $limit = 82;
  if (strlen($text) > $limit) {
    $text = substr($text, 0, $limit-3).'...';
  }
?>
<div class="row question-item">
  <div class="category">
    <span class="glyphicon glyphicon-question-sign"></span>
    <small><?= Yii::t('app', 'Question') ?></small>
  </div>
  <a class="question-text" href="/question/view?id=<?= $question->id ?>"><?= Html::encode($text) ?></a>
  <p class="gray-color">
    <?= Yii::t('app', 'Answered by').':'?>
    <a href="/user/view?id=<?= $question->idProfessional->id ?>"><?= Html::encode($question->idProfessional->shortFullName) ?></a>
  </p>
  <div class="col-md-6">
    <p class="text-center useful">
      <span class="glyphicon glyphicon-ok"></span>
      <small><?= $question['useful'] ?> <?= Yii::t('app', 'Useful') ?></small>
    </p>
  </div>
  <div class="col-md-6">

    <p class="text-center views">
      <span class="glyphicon glyphicon-eye-open"></span>
      <small><?= $question['views'] ?> <?= Yii::t('app', 'Views') ?></small>
    </p>
  </div>
</div>