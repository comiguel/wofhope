<?php

use yii\helpers\Html;
use yii\web\Session;

$session = new Session;
$session->open();
$session['question'] = $session['question'] ?: "";

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="head-search">
        <div class="form-ask">
            <h1 class="text-center"><?= Yii::t('app', 'Psychologists answer your questions anonymously and free') ?></h1>
            <form autocomplete="off" method="post" class="form-inline" action="/question/ask">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                <div class="form-group full-width">
                    <div class="col-sm-8 col-sm-offset-1">
                        <input name="question" type="text" class="form-control full-width" placeholder="<?= Yii::t('app', 'Ask here free') ?>..." value="<?= $session['question']; ?>">
                    </div>
                    <div class="col-sm-2">
                        <p class="text-center"><input class="btn btn-success full-width" type="submit" value="<?= Yii::t('app', 'Ask') ?>"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="container body-content">
        <div class="row">
            <div class="col-md-8">
                <h3 class="text-center text-muted"><?= Yii::t('app', 'Recent articles') ?></h3>
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h3 class="text-center text-muted"><?= Yii::t('app', 'Last answers') ?></h3>
                <?php
                    foreach ($questions as $key => $q) {
                        echo $this->render(
                            '_question.php',
                            ['question' => $q]
                        );
                    }
                ?>
                <!-- <h2>Heading</h2>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
                
                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p> -->
            </div>
        </div>

    </div>
</div>
