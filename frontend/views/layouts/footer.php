<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<footer class="footer">
    <div class="col-md-4">
        <h2 class="text-center">Columna 1</h2>
    </div>
    <div class="col-md-4">
        <h2 class="text-center">Columna 2</h2>
    </div>
    <div class="col-md-4">
        <h2 class="text-center">Columna 3</h2>
    </div>
    <div class="container">
        <strong class="pull-left hidden-xs">&copy; Words Of Hope <?= date('Y') ?></strong>

        <strong class="pull-right">Powered By comiguel</strong>
    </div>
</footer>