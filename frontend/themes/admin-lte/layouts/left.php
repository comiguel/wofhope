<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->request->baseUrl ?>/images/<?= Yii::$app->user->identity->photo ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->getShortFullName(15)?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::t('app', 'Online') ?></a>
            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="<?= Yii::t('app', 'Search') ?>..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => Yii::t('app', 'Menu WOH'), 'options' => ['class' => 'header']],
                    [
                        'label' => Yii::t('app', 'Users'),
                        'icon' => 'fa fa-users',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'List users'), 'url' => ['/user/']],
                            ['label' => Yii::t('app', 'Create user'), 'url' => ['/user/create?role=User']],
                            ['label' => Yii::t('app', 'Create professional'), 'url' => ['/user/create?role=Professional']],
                            ['label' => Yii::t('app', 'Create admin'), 'url' => ['/user/create?role=Administrator']],
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Questions'),
                        'icon' => 'fa fa-question',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'List questions'), 'url' => ['/question/']],
                            // ['label' => Yii::t('app', 'Create question'), 'url' => ['/question/create']],
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Articles'),
                        'icon' => 'fa fa-file-text',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'List articles'), 'url' => ['/article/']],
                            // ['label' => Yii::t('app', 'Create article'), 'url' => ['/article/create']],
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Categories'),
                        'icon' => 'fa fa-map',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('app', 'List categories'), 'url' => ['/category/']],
                            ['label' => Yii::t('app', 'Create category'), 'url' => ['/category/create']],
                        ]
                    ],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => !Yii::$app->user->isGuest],
                    /*[
                        'label' => 'Same tools',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],*/
                ],
            ]
        ) ?>

    </section>

</aside>
