<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents`.
 */
class m161028_042850_create_document_table extends Migration
{
    public function tableName() {
        return "document";
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'description' => $this->text()->notNull(),
            'url' => $this->string(100),
            'id_user' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('document_user', $this->tableName(), 'id_user', 'user', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('document_user', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
