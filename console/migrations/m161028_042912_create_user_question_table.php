<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_question`.
 */
class m161028_042912_create_user_question_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return "user_question";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_question' => $this->integer()->notNull(),
            'action' => $this->string(50)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('user_question_user', $this->tableName(), 'id_user', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('user_question_question', $this->tableName(), 'id_question', 'question', 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('user_question_user', $this->tableName());
        $this->dropForeignKey('user_question_question', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
