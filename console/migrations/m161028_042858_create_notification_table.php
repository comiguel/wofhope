<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents`.
 */
class m161028_042858_create_notification_table extends Migration
{
    public function tableName() {
        return "notification";
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'text' => $this->text(),
            'id_category' => $this->integer()
        ], $tableOptions);
        $this->addForeignKey('notification_category', $this->tableName(), 'id_category', 'category', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('notification_category', $this->tableName());
        $this->dropTable($this->tableName());
    }
}