<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sub_category`.
 */
class m161028_042616_create_sub_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return "sub_category";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'id_category' => $this->integer()->notNull(),
            'id_sub_category' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('sub_category_category', $this->tableName(), 'id_category', 'category', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('sub_category_sub_category', $this->tableName(), 'id_sub_category', 'category', 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('sub_category_category', $this->tableName());
        $this->dropForeignKey('sub_category_sub_category', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
