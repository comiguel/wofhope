<?php

use yii\db\Migration;
use common\models\Status;

/**
 * Handles the creation of table `user`.
 */
class m161028_042711_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName() {
        return 'user';
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'email' => $this->string(100)->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'birthdate' => $this->date()->notNull(),
            'city' => $this->string(100),
            'state' => $this->string(100),
            'country' => $this->string(100),
            'role' => $this->string(50)->notNull(),
            'gender' => $this->string(30)->notNull(),
            'id_status' => $this->integer()->notNull()->defaultValue(Status::USER_INACTIVE),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'access_token' => $this->text(),
            'auth_key' => $this->string(32),
            'push_token' => $this->text(),
            'firstname' => $this->string(100),
            'lastname' => $this->string(100),
            'username' => $this->string(30),
            'photo' => $this->string(70)->defaultValue('default.jpg'),
            'photo_requested' => $this->string(70),
            'profile' => $this->text(),
        ], $tableOptions);
        $this->addForeignKey('user_status', $this->tableName(), 'id_status', 'status', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName());
    }
}
