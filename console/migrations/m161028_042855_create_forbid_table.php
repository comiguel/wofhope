<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents`.
 */
class m161028_042855_create_forbid_table extends Migration
{
    public function tableName() {
        return "forbid";
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'reason' => $this->text(),
            'id_user' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('forbid_user', $this->tableName(), 'id_user', 'user', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('forbid_user', $this->tableName());
        $this->dropTable($this->tableName());
    }
}