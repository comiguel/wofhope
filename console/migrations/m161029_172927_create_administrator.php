<?php

use yii\base\InvalidConfigException;
use yii\rbac\DbManager;
use yii\db\Migration;

class m161029_172927_create_administrator extends Migration
{
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    public function safeUp()
    {
        $authManager = $this->getAuthManager();
        $this->insert('user', [
            'id' => 1,
            'firstname' => 'Administrador',
            'lastname' => 'Del sistema',
            'email' => 'admin@woh.co',
            'photo' => 'admin.png',
            'city' => 'Barranquilla',
            'state' => 'Atlántico',
            'country' => 'Colombia',
            'password' => Yii::$app->security->generatePasswordHash('123456'),
            'password_reset_token' => Yii::$app->security->generateRandomString() . '_' . time(),
            'birthdate' => '1992-08-12',
            'role' => 'Administrator',
            'gender' => 'M',
            'username' => 'admin',
            'id_status' => 1,
            'created_at' => strtotime('now'),
            'updated_at' => strtotime('now'),
            'access_token' => Yii::$app->security->generateRandomString(),
            'auth_key' => Yii::$app->security->generateRandomString()
        ]);

        $this->insert($authManager->assignmentTable, [
            'item_name' => 'Administrator',
            'user_id' => 1,
            'created_at' => strtotime('now'),
        ]);
    }

    public function safeDown()
    {
        $authManager = $this->getAuthManager();
        $this->delete($authManager->assignmentTable, [
            'item_name' => 'Administrator',
            'user_id' => 1
        ]);
        $this->delete('user', ['id' => 1]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
