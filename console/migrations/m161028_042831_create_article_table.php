<?php

use yii\db\Migration;
use common\models\Status;

/**
 * Handles the creation of table `article`.
 */
class m161028_042831_create_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName() {
        return "article";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'image' => $this->string(100),
            'views' => $this->integer()->notNull()->defaultValue(0),
            'useful' => $this->integer()->notNull()->defaultValue(0),
            'timestamp' => $this->timestamp()->notNull(),
            'id_professional' => $this->integer()->notNull(),
            'id_category' => $this->integer()->notNull(),
            'id_status' => $this->integer()->notNull()->defaultValue(Status::ARTICLE_ACTIVE),
        ], $tableOptions);
        $this->addForeignKey('article_professional', $this->tableName(), 'id_professional', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('article_category', $this->tableName(), 'id_category', 'category', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('article_status', $this->tableName(), 'id_status', 'status', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('article_professional', $this->tableName());
        $this->dropForeignKey('article_category', $this->tableName());
        $this->dropForeignKey('article_status', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
