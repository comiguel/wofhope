<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m161028_042409_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return 'status';
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'table' => $this->string(100)->notNull(),
            'description' => $this->text()->notNull()
        ], $tableOptions);

        $this->insert($this->tableName(), [
            'id' => 1,
            'name' => 'Active',
            'table' => 'user',
            'description' => 'Active user'
        ]);

        $this->insert($this->tableName(), [
            'id' => 2,
            'name' => 'Inactive',
            'table' => 'user',
            'description' => 'Inactive user'
        ]);

        $this->insert($this->tableName(), [
            'id' => 3,
            'name' => 'Deleted',
            'table' => 'user',
            'description' => 'Deleted user'
        ]);

        $this->insert($this->tableName(), [
            'id' => 4,
            'name' => 'Active',
            'table' => 'question',
            'description' => 'Active question'
        ]);

        $this->insert($this->tableName(), [
            'id' => 5,
            'name' => 'Inactive',
            'table' => 'question',
            'description' => 'Inactive question'
        ]);

        $this->insert($this->tableName(), [
            'id' => 6,
            'name' => 'Active',
            'table' => 'category',
            'description' => 'Active Category'
        ]);

        $this->insert($this->tableName(), [
            'id' => 7,
            'name' => 'Inactive',
            'table' => 'category',
            'description' => 'Inactive Category'
        ]);

        $this->insert($this->tableName(), [
            'id' => 8,
            'name' => 'Active',
            'table' => 'article',
            'description' => 'Active Article'
        ]);

        $this->insert($this->tableName(), [
            'id' => 9,
            'name' => 'Inactive',
            'table' => 'article',
            'description' => 'Inactive Article'
        ]);

        $this->insert($this->tableName(), [
            'id' => 10,
            'name' => 'Not readed',
            'table' => 'notification',
            'description' => 'Not readed notification'
        ]);

        $this->insert($this->tableName(), [
            'id' => 11,
            'name' => 'Readed',
            'table' => 'notification',
            'description' => 'Readed Notification'
        ]);

        $this->insert($this->tableName(), [
            'id' => 12,
            'name' => 'Not visible',
            'table' => 'notification',
            'description' => 'Not visible Notification'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropTable($this->tableName());
    }
}
