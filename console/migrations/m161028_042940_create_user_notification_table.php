<?php

use yii\db\Migration;
use common\models\Status;

/**
 * Handles the creation of table `user_notification`.
 */
class m161028_042940_create_user_notification_table extends Migration
{
    public function tableName() {
        return "user_notification";
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'id_status' => $this->integer()->notNull()->defaultValue(Status::NOTIFICATION_ACTIVE),
            'id_user' => $this->integer()->notNull(),
            'id_notification' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('user_notification_user', $this->tableName(), 'id_user', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('user_notification_notification', $this->tableName(), 'id_notification', 'notification', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('user_notification_notification', $this->tableName());
        $this->dropForeignKey('user_notification_user', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
