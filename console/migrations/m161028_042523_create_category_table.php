<?php

use yii\db\Migration;
use common\models\Status;

/**
 * Handles the creation of table `category`.
 */
class m161028_042523_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return "category";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(60)->notNull(),
            'id_status' => $this->integer()->notNull()->defaultValue(Status::CATEGORY_ACTIVE)
        ], $tableOptions);
        $this->addForeignKey('category_status', $this->tableName(), 'id_status', 'status', 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('category_status', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
