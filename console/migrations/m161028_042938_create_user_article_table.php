<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_article`.
 */
class m161028_042938_create_user_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return "user_article";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_article' => $this->integer()->notNull(),
            'action' => $this->string(50)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('user_article_user', $this->tableName(), 'id_user', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('user_article_article', $this->tableName(), 'id_article', 'article', 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('user_article_user', $this->tableName());
        $this->dropForeignKey('user_article_article', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
