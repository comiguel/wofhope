<?php

use yii\db\Migration;
use common\models\Status;

/**
 * Handles the creation of table `question`.
 */
class m161028_042802_create_question_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName() {
        return "question";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'information' => $this->text(),
            'answer' => $this->text(),
            'email_answer' => $this->string(100)->notNull(),
            'views' => $this->integer()->notNull()->defaultValue(0),
            'useful' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'answered_at' => $this->integer(),
            'id_asker' => $this->integer(),
            'id_professional' => $this->integer(),
            'id_category' => $this->integer()->defaultValue(0),
            'id_status' => $this->integer()->notNull()->defaultValue(Status::QUESTION_ACTIVE),
        ], $tableOptions);
        $this->addForeignKey('question_asker', $this->tableName(), 'id_asker', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('question_professional', $this->tableName(), 'id_professional', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('question_category', $this->tableName(), 'id_category', 'category', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('question_status', $this->tableName(), 'id_status', 'status', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('question_asker', $this->tableName());
        $this->dropForeignKey('question_professional', $this->tableName());
        $this->dropForeignKey('question_category', $this->tableName());
        $this->dropForeignKey('question_status', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
