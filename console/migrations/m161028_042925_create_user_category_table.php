<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sub_category`.
 */
class m161028_042925_create_user_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
        return "user_category";
    }

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName(), [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_category' => $this->integer()->notNull(),
            'information' => $this->string(200),
        ], $tableOptions);
        $this->addForeignKey('user_category_user', $this->tableName(), 'id_user', 'user', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('user_category_category', $this->tableName(), 'id_category', 'category', 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName());
        $this->dropForeignKey('user_category_user', $this->tableName());
        $this->dropForeignKey('user_category_category', $this->tableName());
        $this->dropTable($this->tableName());
    }
}
