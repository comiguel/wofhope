<?php

use yii\helpers\Html;
use common\models\Status;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create professional'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => ['class' => 'text-center'],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'firstname',
            'lastname',
            'email:email',
            // 'password',
            // 'password_reset_token',
            // 'birthdate',
            // 'role',
            [
                'headerOptions' => ['width' => '12%'],
                'attribute' => 'role',
                'value' => function($value){
                    return Yii::t('app', $value->role);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'role',
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'data' => $roles,
                ]),
            ],
            // 'gender',
            [
                // 'headerOptions' => ['width' => '12%'],
                'attribute' => 'gender',
                'value' => function($value){
                    switch ($value->gender) {
                        case 'M':
                            return Yii::t('app', 'Male');
                            break;
                        case 'F':
                            return Yii::t('app', 'Female');
                            break;
                    }
                },
                // 'filterType' => GridView::FILTER_SELECT2,
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'gender',
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'data' => ['M' => Yii::t('app', 'Male'), 'F' => Yii::t('app', 'Female')],
                ]),
            ],
            // 'username',
            // 'photo',
            // 'id_status',
            [
                'headerOptions' => ['width' => '12%'],
                'attribute' => 'id_status',
                'value' => function($value){
                    switch ($value->id_status) {
                        case Status::USER_ACTIVE:
                            return Yii::t('app', 'Active');
                            break;
                        case Status::USER_INACTIVE:
                            return Yii::t('app', 'Inactive');
                            break;
                        case Status::USER_DELETED:
                            return Yii::t('app', 'Deleted');
                            break;
                    }
                },
                // 'filterType' => GridView::FILTER_SELECT2,
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_status',
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'data' => [Status::USER_ACTIVE => Yii::t('app', 'Active'), Status::USER_INACTIVE => Yii::t('app', 'Inactive'), Status::USER_DELETED => Yii::t('app', 'Deleted')],
                ]),
            ],
            // 'created_at',
            // 'updated_at',
            // 'access_token:ntext',
            // 'auth_key',
            // 'push_token:ntext',


            ['class' => 'yii\grid\ActionColumn'],
        ],
        'toolbar' => [
            ['content'=>
                Html::a(Yii::t('app', 'Clean filters'), ['index'], ['class' => 'btn btn-primary'])
            ],
            '{export}',
            // '{toggleData}',

        ],
        'exportConfig' => [
            GridView::EXCEL => [
                'label' => 'Excel',
                'icon' => 'floppy-remove',
                'iconOptions' => ['class' => 'text-success'],
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'filename' => 'Reporte de usuarios WOH '.date('d-m-Y'),
                'alertMsg' => 'Se va a generar el archivo de EXCEL para la descarga.',
                'options' => ['title' => 'Microsoft Excel 95+'],
                'mime' => 'application/vnd.ms-excel',
                'config' => [
                    'worksheet' => 'HojaReporte',
                    'cssFile' => ''
                ]
            ],
            GridView::PDF => [
                'label' => 'PDF',
                'icon' => 'floppy-disk',
                'iconOptions' => ['class' => 'text-danger'],
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'filename' => 'Reporte de usuarios WOH '.date('d-m-Y'),
                'alertMsg' => 'Se va a generar el reporte en formato PDF',
                'options' => ['title' => 'Formato de Documento Portable'],
                'mime' => 'application/pdf',
                'config' => [
                    'mode' => 'c',
                    'format' => 'A4-L',
                    'destination' => 'D',
                    'marginTop' => 20,
                    'marginBottom' => 20,
                    'cssInline' => '.kv-wrap{padding:20px;}' .
                        '.kv-align-center{text-align:center;}' .
                        '.kv-align-left{text-align:left;}' .
                        '.kv-align-right{text-align:right;}' .
                        '.kv-align-top{vertical-align:top!important;}' .
                        '.kv-align-bottom{vertical-align:bottom!important;}' .
                        '.kv-align-middle{vertical-align:middle!important;}' .
                        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                    'methods' => [
                        'SetHeader' => ['Reporte de WOH||Generado el: ' . date("d-m-Y")],
                        'SetFooter' => ['Powered By Blasuca||Página {PAGENO}'],
                    ],
                    'options' => [
                        'title' => 'Reporte de usuarios WOH',
                        'subject' => 'PDF de reporte con los usuarios',
                        'keywords' => 'TA, Reporte, WOH, pdf'
                    ],
                    'contentBefore'=>'',
                    'contentAfter'=>''
                ]
            ],
        ],
        'hover' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => '<i class="glyphicon glyphicon-list-alt"></i> '.Yii::t('app', 'Users list'),
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
