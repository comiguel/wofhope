<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->shortFullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $status = Yii::t('app', $model->idStatus->name);
        $role = Yii::t('app', $model->role);
        $attributes = ['firstname',
        'lastname',
        'username',
        'email:email',
        'birthdate',
        'city',
        'state',
        'country',
        [
            'attribute' => 'role',
            'value' => $role
        ],
        [
            'attribute' => 'gender',
            'value' => $model->prettyGender
        ],
        'profile:ntext',
        [
            'attribute' => 'id_status',
            'value' => $status
        ],
        [
            'attribute' => 'created_at',
            'value' => $model->getPrettyDate($model->created_at)
        ],
        [
            'attribute' => 'updated_at',
            'value' => $model->getPrettyDate($model->updated_at)]
        ];
        if($model->role !== User::PROFESSIONAL_ROLE) {
            $todelete = ['username', 'profile:ntext'];
            foreach ($todelete as $key => $value) {
                unset($attributes[array_search($value, $attributes, true)]);
            }
        }
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $attributes,
        ]) ?>

</div>
