<?php
if (!$model->isNewRecord) {
?>
    <script>
        $(document).ready(function() {
            $('#user-role').on('change', function(event) {
                event.preventDefault();
                window.location.replace("/user/update?id=<?= Yii::$app->request->get('id');?>&change="+$(this).val());
            });
        });
    </script>
<?php
}

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        if (!$model->isNewRecord) {
            echo $form->field($model, 'role')->widget(Select2::classname(),[
                'initValueText' => $model->isNewRecord ? '' : $model->role,
                // 'options' => ['placeholder' => Yii::t('app','Select a role')],
                'hideSearch' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'options' => ['prompt' => ''],
                'data' => $roles,
            ]);
        }
    ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'value' => '']) ?>

    <!-- <?// $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?> -->

    <!-- <?= $form->field($model, 'birthdate')->textInput() ?> -->
    <div class="form-group field-user-birthdate">
        <label class="control-label" for="user-birthdate"><?= Yii::t('app', 'Birthdate') ?></label>
            <?= DateRangePicker::widget([
                'model' => $model,
                'attribute' => 'birthdate',
                // 'containerTemplate' => '<div class="col-md-12">{input}</div>',
                'convertFormat'=>true,
                'options' => [
                    // 'placeholder' => Yii::t('app', 'Birthday'),
                ],
                'pluginOptions'=>[
                    'startDate'=>date('Y-m-d', strtotime("-20 years")),
                    'minDate'=>date('Y-m-d', strtotime("-70 years")),
                    'maxDate'=>date('Y-m-d', strtotime("-18 years")),
                    'showDropdowns'=>true,
                    'singleDatePicker'=>true,
                    'timePicker'=>false,
                    // 'allowClear' => true,
                    // 'timePickerIncrement'=>30,
                    'format'=>'Y-m-d',
                ],
            ]) ?>
            <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'gender')->widget(Select2::classname(),[
        'initValueText' => $model->isNewRecord ? '' : $model->gender,
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'hideSearch' => true,
        'options' => ['prompt' => ''],
        'data' => ['M' => Yii::t('app','Male'), 'F' => Yii::t('app','Female')],
    ]);?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?php
        if(!$model->isNewRecord) {
            echo $form->field($model, 'id_status')->widget(Select2::classname(),[
                'initValueText' => $model->isNewRecord ? '' : $model->id_status,
                'hideSearch' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
                'options' => ['prompt' => ''],
                'data' => $statuses,
            ]);
        }
    ?>

    <?= $form->field($model, 'profile')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
