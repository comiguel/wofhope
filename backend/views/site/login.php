<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\checkbox\CheckboxX;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row" style="padding:8% 0">
        <div class="col-md-6 col-md-offset-3">
            <p class="text-center"><img src="<?= Yii::$app->request->baseUrl; ?>/images/logo.png" alt="WordsOfHope"></p>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
            ]);
            ?>
                <?= $form->field($model, 'email', ['template' => '<div class="col-md-10 col-md-offset-1">{input}{error}</div>', 'options' => ['class' => 'form-group']])->textInput(['placeholder' => Yii::t('app', 'Email'),'maxlength' => 45])?>
                <?= $form->field($model, 'password', ['template' => '<div class="col-md-10 col-md-offset-1">{input}{error}</div>', 'options' => ['class' => 'form-group']])->passwordInput(['placeholder' => Yii::t('app', 'Password'),'maxlength' => 45])?>
                <?= $form->field($model, 'rememberMe', ['template' => '{label}<div class="col-md-1 col-md-offset-1">{input}{error}</div>', 'options' => ['class' => 'form-group']])->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false, 'size' => 'lg']])?>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" name="login-button" class="btn btn-lg btn-block btn-success"><?= Yii::t('app', 'Login')?></button>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

