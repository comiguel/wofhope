<?php

use yii\helpers\Html;
use common\models\Status;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => ['class' => 'text-center'],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            [
                'headerOptions' => ['width' => '12%'],
                'attribute' => 'id_status',
                'value' => function($value) {
                    switch ($value->id_status) {
                        case Status::CATEGORY_ACTIVE:
                            return Yii::t('app', 'Active');
                            break;
                        case Status::CATEGORY_INACTIVE:
                            return Yii::t('app', 'Inactive');
                            break;
                    }
                },
                // 'filterType' => GridView::FILTER_SELECT2,
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id_status',
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'data' => [Status::CATEGORY_ACTIVE => Yii::t('app', 'Active'), Status::CATEGORY_INACTIVE => Yii::t('app', 'Inactive')],
                ]),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'toolbar' => [
            ['content'=>
                Html::a(Yii::t('app', 'Clean filters'), ['index'], ['class' => 'btn btn-primary'])
            ],
            // '{export}',
            // '{toggleData}',

        ],
        'hover' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => '<i class="glyphicon glyphicon-list-alt"></i> '.Yii::t('app', 'Categories list'),
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
