<script>
  $(document).ready(function() {
    $('#category-issubcategory').on('change', function(event) {
      event.preventDefault();
      if ($(this).val() === '1') {
        $('#category-parent').removeAttr('disabled');
      } else {
        $('#category-parent').attr('disabled', true);
      }
    });
  });
</script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'isSubCategory'/*, ['template' => '{label}<div class="col-md-1 col-md-offset-1">{input}{error}</div>', 'options' => ['class' => 'form-group']]*/)->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false, 'size' => 'lg']])?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
    $initValueText = "";
      if ($model->isNewRecord){
        $initValueText = '';
      } elseif ($model->parentCategories) {
        $initValueText = $model->parentCategories[0]->category->name;
        // ->asArray
        // ->one()['name'];
      }
      echo $form->field($model, 'parent')->widget(Select2::classname(),[
        'initValueText' => $initValueText,
        // 'hideSearch' => true,
        'pluginOptions' => [
            'allowClear' => true,
        ],
        'options' => ['prompt' => '', 'disabled' => $model->isSubCategory ? false : true],
        'data' => $categories,
    ]);?>

    <?php
      if(!$model->isNewRecord) {
        echo $form->field($model, 'id_status')->widget(Select2::classname(),[
          // 'initValueText' => $model->isNewRecord ? '' : $model->id_status,
          'hideSearch' => true,
          'pluginOptions' => [
            'allowClear' => true,
          ],
          'options' => ['prompt' => ''],
          'data' => $statuses,
        ]);
      }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
