<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->request->baseUrl; ?>/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= Yii::$app->request->baseUrl; ?>/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::$app->request->baseUrl; ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::$app->request->baseUrl; ?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->request->baseUrl; ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= Yii::$app->request->baseUrl; ?>/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Words Of Hope',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        // ['label' => 'Home', 'url' => ['/site/index']],
    ];
    // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    $menuItems[] = [
        'label' => Yii::t('app', 'Users'),
        'items' => [
            ['label' => Yii::t('app', 'List users'), 'url' => ['/user/']],
            ['label' => Yii::t('app', 'Create user'), 'url' => ['/user/create?role=User']],
            ['label' => Yii::t('app', 'Create professional'), 'url' => ['/user/create?role=Professional']],
            ['label' => Yii::t('app', 'Create admin'), 'url' => ['/user/create?role=Administrator']],
        ]
    ];
    $menuItems[] = [
        'label' => Yii::t('app', 'Questions'),
        'items' => [
            ['label' => Yii::t('app', 'List questions'), 'url' => ['/question/']],
            // ['label' => Yii::t('app', 'Create question'), 'url' => ['/question/create']],
        ]
    ];
    $menuItems[] = [
        'label' => Yii::t('app', 'Articles'),
        'items' => [
            ['label' => Yii::t('app', 'List articles'), 'url' => ['/article/']],
            // ['label' => Yii::t('app', 'Create article'), 'url' => ['/article/create']],
        ]
    ];
    $menuItems[] = [
        'label' => Yii::t('app', 'Categories'),
        'items' => [
            ['label' => Yii::t('app', 'List categories'), 'url' => ['/category/']],
            ['label' => Yii::t('app', 'Create category'), 'url' => ['/category/create']],
        ]
    ];
    /*$menuItems[] = [
    'label' => Yii::t('app', 'Statuses'),
        'items' => [
            ['label' => Yii::t('app', 'List statuses'), 'url' => ['/status/']],
            // ['label' => Yii::t('app', 'Create status'), 'url' => ['/status/create']],
        ]
    ];*/
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            Yii::t('app', 'Logout').' (' . Yii::$app->user->identity->email . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Words Of Hope <?= date('Y') ?></p>

        <p class="pull-right">Powered By comiguel</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
