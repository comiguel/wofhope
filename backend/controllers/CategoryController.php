<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Category;
use common\models\CategorySearch;
use common\models\SubCategory;
use common\models\Status;
use common\models\Functions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\Query;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ADMINISTRATOR_ROLE],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            // var_dump($model);echo "<br>";die();
            if (!$model->isSubCategory) {
                $model->parent = "NO";
            }
            if ($model->save()) {
                if ($model->isSubCategory) {
                    $subCategory = new SubCategory();
                    $subCategory->id_sub_category = $model->id;
                    $subCategory->id_category = $model->parent;
                    $subCategory->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        $categories = $this->getCategories();
        return $this->render('create', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $parent = $model->parentCategories;
        if (count($parent) > 0) {
            $model->isSubCategory = '1';
            $model->parent = $parent[0]->id_category;
        } else {
            $model->parent = "NO";
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $categories = $this->getCategories(false, $model->id);
            $statuses = Functions::getStatuses('category');
            return $this->render('update', [
                'model' => $model,
                'categories' => $categories,
                'statuses' => $statuses,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $model = $this->findModel($id);
        // $model->id_status = Status::CATEGORY_INACTIVE;
        // $model->save(false);
        $query = (new Query())
        ->select('id_sub_category')
        ->from('sub_category')
        ->where(['id_category' => $id]);

        Yii::$app->db->createCommand()
        ->update('category',
            ['id_status' => Status::CATEGORY_INACTIVE],
            ['or', ['id' => $query], ['id' => $id]])
        ->execute();

        return $this->redirect(['index']);
    }

    protected function getCategories($creating = true, $id = 0)
    {
        $query = new Query();
        $categories = $query->select(['id', 'name'])
        ->from('category');
        if (!$creating) {
            $categories->where(['not', ['id' => $id]]);
        }
        $categories = $categories->andWhere(['id_status' => Status::CATEGORY_ACTIVE])->all();
        // $categories = ArrayHelper::map($categories, 'id', 'name');
        $traduced = [];
        foreach ($categories as $key => $value) {
            $traduced[$value['id']] = Yii::t('app', $value['name']);
        }
        return $traduced;
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
