<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Functions;

/**
 * User model
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $password_reset_token
 * @property string $birthdate
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $role
 * @property string $gender
 * @property string $name
 * @property string $username
 * @property string $photo
 * @property string $photo_requested
 * @property string $profile
 * @property integer $id_status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $access_token
 * @property string $auth_key
 * @property string $push_token
 * @property string $password write-only password
 * @property Article[] $articles
 * @property Document[] $documents
 * @property Forbid[] $forbs
 * @property Question[] $asked
 * @property Question[] $answers
 * @property Status $idStatus
 * @property UserArticle[] $userArticles
 * @property UserCategory[] $userCategories
 * @property UserNotification[] $userNotifications
 * @property UserQuestion[] $userQuestions
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_CREATING = 'creating';
    const SCENARIO_CREATING_USER = 'creating_user';
    const SCENARIO_UPDATING = 'updating';
    const SCENARIO_UPDATING_USER = 'updating_user';
    const USER_ROLE = 'User';
    const PROFESSIONAL_ROLE = 'Professional';
    const ADMINISTRATOR_ROLE = 'Administrator';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function setScenarioFromRole($creating = false, $role = null)
    {
        $role = $role ?: $this->role;
        switch ($role) {
            case self::USER_ROLE:
                if($creating) {
                    $this->scenario = self::SCENARIO_CREATING_USER;
                    $this->role = $role;
                } else {
                    $this->scenario = self::SCENARIO_UPDATING_USER;
                }
                break;
            case self::ADMINISTRATOR_ROLE:
                if($creating) {
                    $this->scenario = self::SCENARIO_CREATING_USER;
                    $this->role = $role;
                } else {
                    $this->scenario = self::SCENARIO_UPDATING_USER;
                }
                break;
            case self::PROFESSIONAL_ROLE:
                if($creating) {
                    $this->scenario = self::SCENARIO_CREATING;
                    $this->role = $role;
                } else {
                    $this->scenario = self::SCENARIO_UPDATING;
                }
            break;
            default:
                if($creating) {
                    $this->scenario = self::SCENARIO_CREATING;
                    $this->role = self::PROFESSIONAL_ROLE;
                }
            break;
        }
    }

    public function getCustomScenarios()
    {
        $scenarios = [
            self::SCENARIO_CREATING => ['id', 'email', 'password', 'password_reset_token', 'birthdate', 'city', 'state', 'country', 'role', 'gender', 'id_status', 'access_token', 'auth_key', 'push_token', 'firstname', 'lastname', 'username', 'profile'],
            self::SCENARIO_UPDATING => ['firstname', 'lastname', 'username', 'email', 'password', 'password_reset_token', 'birthdate', 'city', 'state', 'country', 'role', 'gender', 'id_status', 'access_token', 'auth_key', 'push_token', 'profile'],
      ];
      $scenarios[self::SCENARIO_CREATING_USER] = $scenarios[self::SCENARIO_CREATING];
      $scenarios[self::SCENARIO_UPDATING_USER] = $scenarios[self::SCENARIO_UPDATING];
      return $scenarios;
    }

    public function ModifyRequired()
    {
        $allscenarios = $this->getCustomScenarios();
        $not_required_creating = ['firstname', 'lastname', 'username', 'profile'];
        $allscenarios[self::SCENARIO_CREATING] = array_diff($allscenarios[self::SCENARIO_CREATING], ['id', 'password_reset_token', 'city', 'state', 'country', 'auth_key', 'push_token']);
        $allscenarios[self::SCENARIO_CREATING_USER] = array_diff($allscenarios[self::SCENARIO_CREATING], $not_required_creating);
        $allscenarios[self::SCENARIO_UPDATING] = array_diff($allscenarios[self::SCENARIO_UPDATING], ['password', 'password_reset_token', 'city', 'state', 'country', 'auth_key', 'push_token']);
        $allscenarios[self::SCENARIO_UPDATING_USER] = array_diff($allscenarios[self::SCENARIO_CREATING], $not_required_creating, ['password']);
        return $allscenarios;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios = $this->getCustomScenarios();
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $allscenarios = $this->ModifyRequired();
        return [
            [$allscenarios[self::SCENARIO_CREATING], 'required', 'on' => self::SCENARIO_CREATING],
            [$allscenarios[self::SCENARIO_CREATING_USER], 'required', 'on' => self::SCENARIO_CREATING_USER],
            [$allscenarios[self::SCENARIO_UPDATING], 'required', 'on' => self::SCENARIO_UPDATING],
            [$allscenarios[self::SCENARIO_UPDATING_USER], 'required', 'on' => self::SCENARIO_UPDATING_USER],
            ['id_status', 'default', 'value' => Status::USER_ACTIVE],
            ['id_status', 'in', 'range' => [Status::USER_ACTIVE, Status::USER_INACTIVE, Status::USER_DELETED]],
            [['birthdate'], 'safe'],
            [['id_status', 'created_at', 'updated_at'], 'integer'],
            [['access_token', 'push_token', 'profile'], 'string'],
            [['email', 'city', 'state', 'country', 'firstname', 'lastname'], 'string', 'max' => 100],
            [['password', 'password_reset_token'], 'string', 'max' => 255],
            [['role'], 'string', 'max' => 50],
            [['gender', 'username'], 'string', 'max' => 30],
            [['photo', 'photo_requested'], 'string', 'max' => 70],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'email'],
            [['email', 'password_reset_token'], 'unique'],
            // [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'country' => Yii::t('app', 'Country'),
            'role' => Yii::t('app', 'Role'),
            'gender' => Yii::t('app', 'Gender'),
            'id_status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'access_token' => Yii::t('app', 'Access Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'push_token' => Yii::t('app', 'Push Token'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'username' => Yii::t('app', 'Username'),
            'photo' => Yii::t('app', 'Photo'),
            'photo_requested' => Yii::t('app', 'Photo Requested'),
            'profile' => Yii::t('app', 'Professional profile'),
        ];
    }

    public function getShortFullName($max = 100)
    {
        $full_name = Functions::cutName($this->firstname)
            .' '.
            Functions::cutName($this->lastname);
        if (strlen($full_name) >= $max) {
            $full_name = Functions::cutName($full_name);
        }
        return $full_name;
    }

    public function getPrettyGender()
    {
        switch ($this->gender) {
            case 'M':
                return Yii::t('app', 'Male');
                break;
            case 'F':
                return Yii::t('app', 'Female');
                break;
        }
    }

    public function getPrettyDate($date)
    {
        return date('Y-m-d H:i:s', $date);
    }

    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id_professional' => 'id']);
    }

    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['id_user' => 'id']);
    }

    public function getForbs()
    {
        return $this->hasMany(Forbid::className(), ['id_user' => 'id']);
    }

    public function getAsked()
    {
        return $this->hasMany(Question::className(), ['id_asker' => 'id']);
    }

    public function getAnswers()
    {
        return $this->hasMany(Question::className(), ['id_professional' => 'id']);
    }

    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    public function getUserArticles()
    {
        return $this->hasMany(UserArticle::className(), ['id_user' => 'id']);
    }

    public function getUserCategories()
    {
        return $this->hasMany(UserCategory::className(), ['id_user' => 'id']);
    }

    public function getUserNotifications()
    {
        return $this->hasMany(UserNotification::className(), ['id_user' => 'id']);
    }

    public function getUserQuestions()
    {
        return $this->hasMany(UserQuestion::className(), ['id_user' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'id_status' => Status::USER_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        return static::findOne(['access_token' => $access_token, 'id_status' => Status::USER_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'id_status' => Status::USER_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'id_status' => Status::USER_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'id_status' => Status::USER_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
