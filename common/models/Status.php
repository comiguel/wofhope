<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 * @property string $table
 * @property string $description
 *
 * @property Article[] $articles
 * @property Category[] $categories
 * @property Question[] $questions
 * @property User[] $users
 */
class Status extends \yii\db\ActiveRecord
{
    const USER_ACTIVE = 1;
    const USER_INACTIVE = 2;
    const USER_DELETED = 3;
    const QUESTION_ACTIVE = 4;
    const QUESTION_INACTIVE = 5;
    const QUESTION_DELETED = 6;
    const CATEGORY_ACTIVE = 7;
    const CATEGORY_INACTIVE = 8;
    const ARTICLE_ACTIVE = 9;
    const ARTICLE_INACTIVE = 10;
    const NOTIFICATION_ACTIVE = 11;
    const NOTIFICATION_READED = 12;
    const NOTIFICATION_INACTIVE = 13;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'table', 'description'], 'required'],
            [['description'], 'string'],
            [['name', 'table'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'table' => Yii::t('app', 'Table'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id_status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_status' => 'id']);
    }
}
