<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Functions extends Model
{
  public static function csv_to_array($filename='', $delimiter=',', $head = NULL)
  {
    if(!file_exists($filename) || !is_readable($filename)){
      return FALSE;
    }

    if(!$head){
      $header = NULL;
    }else{
      $header = str_getcsv($head, $delimiter);
    }
    $data = [];
    if (($handle = fopen($filename, 'r')) !== FALSE){
      while (($row2 = fgetcsv($handle, 0, $delimiter)) !== FALSE)
      {
        $row = Functions::utf8_converter($row2);
        if(!$header){
          $header = $row;
        }else{
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }
    return $data;
    // return json_encode($data);
  }

  public static function utf8_converter($array)
  {
    array_walk_recursive($array, function(&$item, $key){
      if(!mb_detect_encoding($item, 'utf-8', true)){
        $item = utf8_encode($item);
      }
    });
    return $array;
  }

  public static function cutName($name)
  {
    $custom_name = explode(' ', $name);
    $result = '';
    if(count($custom_name) > 2){
      if(strlen($custom_name[0]) < 4){
        if(strlen($custom_name[1]) < 4){
          $result .= $custom_name[0].' '.$custom_name[1].' '.$custom_name[2];
        }else{
          $result .= $custom_name[0].' '.$custom_name[1];
        }
      }elseif(strlen($custom_name[1]) < 4){
        $result .= $custom_name[0];
      }else{
        $result .= $custom_name[0].' '.$custom_name[1];
      }
    }elseif(count($custom_name) > 1){
      if(strlen($custom_name[0]) < 4){
          $result .= $custom_name[0].' '.$custom_name[1];
      }else{
        $result .= $custom_name[0];
      }
    }else{
      $result .= $custom_name[0];
    }
    return $result;
  }

  public static function getItems()
  {
    $query = new Query();
    $roles = $query->select('name')
    ->from('items')
    ->all();
        // $roles = ArrayHelper::map($roles, 'name', 'name');
    $traduced = [];
    foreach ($roles as $key => $value) {
      $traduced[$value['name']] = Yii::t('app', $value['name']);
    }
    return $traduced;
  }

  public static function getStatuses($table)
  {
    $query = new Query();
    $statuses = $query->select(['id', 'name'])
    ->from('status')
    ->where(['table' => $table])
    ->all();
        // $statuses = ArrayHelper::map($statuses, 'id', 'name');
    $traduced = [];
    foreach ($statuses as $key => $value) {
      $traduced[$value['id']] = Yii::t('app', $value['name']);
    }
    return $traduced;
  }
}

?>