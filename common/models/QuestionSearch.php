<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Question;

/**
 * QuestionSearch represents the model behind the search form about `common\models\Question`.
 */
class QuestionSearch extends Question
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'views', 'useful', 'created_at', 'updated_at', 'id_asker', 'id_professional', 'id_category', 'id_status'], 'integer'],
            [['text', 'information', 'answer', 'email_answer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Question::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'views' => $this->views,
            'useful' => $this->useful,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id_asker' => $this->id_asker,
            'id_professional' => $this->id_professional,
            'id_category' => $this->id_category,
            'id_status' => $this->id_status,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'answer', $this->answer])
            ->andFilterWhere(['like', 'email_answer', $this->email_answer]);

        return $dataProvider;
    }
}
