<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_status
 *
 * @property Article[] $articles
 * @property Status $idStatus
 * @property Notification[] $notifications
 * @property Question[] $questions
 * @property SubCategory[] $subCategories
 * @property ParentCategory[] $parentCategories
 * @property UserCategory[] $userCategories
 */
class Category extends \yii\db\ActiveRecord
{
    public $isSubCategory;
    public $parent;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'parent', 'id_status'], 'required'],
            [['isSubCategory', 'parent'], 'string'],
            [['id_status'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'isSubCategory' => Yii::t('app', 'Is subcategory'),
            'parent' => Yii::t('app', 'Parent category'),
            'id_status' => Yii::t('app', 'Status'),
        ];
    }

    public function getCategories($creating = true, $id = 0)
    {
        $query = new Query();
        $categories = $query->select(['id', 'name'])
        ->from('category');
        if (!$creating) {
            $categories->where(['not', ['id' => $id]]);
        }
        $categories = $categories->andWhere(['id_status' => Status::CATEGORY_ACTIVE])->all();
        // $categories = ArrayHelper::map($categories, 'id', 'name');
        $traduced = [];
        foreach ($categories as $key => $value) {
            $traduced[$value['id']] = Yii::t('app', $value['name']);
        }
        return $traduced;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategories()
    {
        return $this->hasMany(SubCategory::className(), ['id_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategories()
    {
        return $this->hasMany(SubCategory::className(), ['id_sub_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCategories()
    {
        return $this->hasMany(UserCategory::className(), ['id_category' => 'id']);
    }
}
