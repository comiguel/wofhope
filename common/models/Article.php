<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $text
 * @property string $image
 * @property integer $views
 * @property integer $useful
 * @property string $timestamp
 * @property integer $id_professional
 * @property integer $id_category
 * @property integer $id_status
 *
 * @property Category $idCategory
 * @property User $idProfessional
 * @property Status $idStatus
 * @property UserArticle[] $userArticles
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'id_professional', 'id_category'], 'required'],
            [['text'], 'string'],
            [['views', 'useful', 'id_professional', 'id_category', 'id_status'], 'integer'],
            [['timestamp'], 'safe'],
            [['image'], 'string', 'max' => 100],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['id_category' => 'id']],
            [['id_professional'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_professional' => 'id']],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
            'views' => Yii::t('app', 'Views'),
            'useful' => Yii::t('app', 'Useful'),
            'timestamp' => Yii::t('app', 'Timestamp'),
            'id_professional' => Yii::t('app', 'Id Professional'),
            'id_category' => Yii::t('app', 'Id Category'),
            'id_status' => Yii::t('app', 'Id Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProfessional()
    {
        return $this->hasOne(User::className(), ['id' => 'id_professional']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserArticles()
    {
        return $this->hasMany(UserArticle::className(), ['id_article' => 'id']);
    }
}
