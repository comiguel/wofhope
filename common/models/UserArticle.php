<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_article".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_article
 * @property string $action
 *
 * @property Article $idArticle
 * @property User $idUser
 */
class UserArticle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_article', 'action'], 'required'],
            [['id_user', 'id_article'], 'integer'],
            [['action'], 'string', 'max' => 50],
            [['id_article'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['id_article' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_article' => Yii::t('app', 'Id Article'),
            'action' => Yii::t('app', 'Action'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'id_article']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
