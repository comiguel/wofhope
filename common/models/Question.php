<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property string $text
 * @property string $information
 * @property string $answer
 * @property string $email_answer
 * @property integer $views
 * @property integer $useful
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $answered_at
 * @property integer $id_asker
 * @property integer $id_professional
 * @property integer $id_category
 * @property integer $id_status
 *
 * @property User $idAsker
 * @property Category $idCategory
 * @property User $idProfessional
 * @property Status $idStatus
 * @property UserQuestion[] $userQuestions
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'email_answer'], 'required'],
            [['text', 'information', 'answer'], 'string'],
            [['views', 'useful', 'created_at', 'updated_at', 'id_asker', 'id_professional', 'id_category', 'id_status'], 'integer'],
            [['email_answer'], 'email'],
            [['id_asker'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_asker' => 'id']],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['id_category' => 'id']],
            [['id_professional'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_professional' => 'id']],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'information' => Yii::t('app', 'Additional information'),
            'answer' => Yii::t('app', 'Answer'),
            'email_answer' => Yii::t('app', 'Email for reply'),
            'views' => Yii::t('app', 'Views'),
            'useful' => Yii::t('app', 'Useful'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'answered_at' => Yii::t('app', 'Answered At'),
            'id_asker' => Yii::t('app', 'Id Asker'),
            'id_professional' => Yii::t('app', 'Id Professional'),
            'id_category' => Yii::t('app', 'Id Category'),
            'id_status' => Yii::t('app', 'Id Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAsker()
    {
        return $this->hasOne(User::className(), ['id' => 'id_asker']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProfessional()
    {
        return $this->hasOne(User::className(), ['id' => 'id_professional']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserQuestions()
    {
        return $this->hasMany(UserQuestion::className(), ['id_question' => 'id']);
    }
}
