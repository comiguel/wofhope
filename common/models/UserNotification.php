<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_notification".
 *
 * @property integer $id
 * @property integer $id_status
 * @property integer $id_user
 * @property integer $id_notification
 *
 * @property Notification $idNotification
 * @property User $idUser
 */
class UserNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_status', 'id_user', 'id_notification'], 'integer'],
            [['id_user', 'id_notification'], 'required'],
            [['id_notification'], 'exist', 'skipOnError' => true, 'targetClass' => Notification::className(), 'targetAttribute' => ['id_notification' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_status' => Yii::t('app', 'Id Status'),
            'id_user' => Yii::t('app', 'User'),
            'id_notification' => Yii::t('app', 'Notification'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNotification()
    {
        return $this->hasOne(Notification::className(), ['id' => 'id_notification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
